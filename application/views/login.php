<!-- ======= Contact Section ======= -->
<section id="contact" class="contact mt-5">
      <div class="container" data-aos="fade-up">

			<div class="section-title">
				<h2>Login</h2>
			</div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">

			<div class="col-lg-3">
			
			</div>
			<div class="col-lg-6 justify-content-lg-center">
			<form action="" method="POST" class="form-custom">
              <div class="form-group">
				<h6>Email</h6>
					<input type="text" class="form-control" name="email"  value="<?= set_value('email'); ?>" autofocus>
					<small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('nama_siswa'); ?></small>
			  </div>
              <div class="form-group pt-4">
				<h6>Password</h6>
					<input type="password" class="form-control" name="password">
					<small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('password'); ?></small>
			  </div>

              <div class="form-group pt-4">
				<h6>Captcha</h6>
					<span><?= $captcha_image; ?></span>
					<a href="" onclick="parent.window.location.reload(true)">Perbarui Gambar</a>
					<input type="text" class="form-control" name="captcha">
			  </div>

			<button type="submit" class="mt-3">Login</button>
		</form>
	</div>

	<div class="col-lg-3">
			
	</div>

        </div>

      </div>
    </section><!-- End Contact Section -->