<!-- ======= Breadcrumbs ======= -->
<section class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="<?= base_url('Page/listEskul') ?>">List Esktrakulikuler</a></li>
          <li>Detail Esktrakulikuler</li>
        </ol>
        <h2>Detail Esktrakulikuler</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container" data-aos="fade-up">
        <div class="portfolio-details-container">
        <?php foreach($eskul as $data) : ?>
          <div class="owl-carousel portfolio-details-carousel">
          <a href="<?= base_url('assets/images/eskul_images/'.$data->item); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->item; ?>">
            <img src="<?= base_url('assets/images/eskul_images/'.$data->item); ?>" class="img-fluid" alt="">
          </a>
        </div>
        <?php endforeach; ?>
          <!-- <div class="portfolio-info">
            <h3>Informasi Kelas</h3>
            <ul>
              <li><strong>Nama Kelas</strong>: <?= $data->judul ?></li>
              <li><strong>Deskripsi Kelas</strong>: <?= $data->deskripsi ?></li>
            </ul>
          </div> -->

        </div>

        <?php foreach($eskul as $data) : ?>
        <div class="portfolio-description">
          <h2><?= $data->judul ?></h2>
          <p>
          <?= $data->deskripsi ?>
          </p>
        </div>
        <?php endforeach; ?>
      </div>
    </section><!-- End Portfolio Details Section -->