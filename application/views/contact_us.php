<!-- ======= Contact Section ======= -->
<section id="contact" class="contact mt-5">
      <div class="container" data-aos="fade-up">

		<?php foreach($beranda as $data) : ?>
			<div class="section-title">
				<h2>Hubungi Kami</h2>
				<p>Anda bisa berikan tanggapan pada <?= $data->nama_sekolah; ?>.</p>
			</div>
		<?php endforeach; ?>

        <div class="row" data-aos="fade-up" data-aos-delay="100">

			
			<div class="col-lg-6">
			<form action="" class="form-custom" method="POST">
              <div class="form-group">
				<h6>Nama</h6>
					<input type="text" class="form-control" name="nama"  value="<?= set_value('nama'); ?>">
					<small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('nama'); ?></small>
			  </div>
			  <div class="form-group">
				<h6>Email</h6>
					<input type="text" class="form-control" name="email"  value="<?= set_value('email'); ?>">
					<small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('email'); ?></small>
			  </div>
			  <div class="form-group">
			  <h6>Pesan</h6>
				<textarea class="form-control" name="pesan" rows="5" data-rule="required"></textarea>
			  </div>	
			<button type="submit">Kirim</button>
		</form>
	</div>
	
		<div class="col-lg-6">
			<?php foreach($contact_us as $data) : ?>
			<div class="row">
			<div class="col-md-12">
			<div class="info-box">
				<i class="bx bx-map"></i>
				<h3>Alamat Kami</h3>
				<p><?= $data->alamat ?></p>
			</div>
			</div>
			<div class="col-md-6">
			<div class="info-box mt-4">
				<i class="bx bx-envelope"></i>
				<h3>Email</h3>
				<p><?= $data->email_sekolah ?></p>
			</div>
			</div>
			<div class="col-md-6">
			<div class="info-box mt-4">
				<i class="bx bx-phone-call"></i>
				<h3>Kontak</h3>
				<p><?= $data->telepon ?></p>
			</div>
			</div>	
		</div>
			<?php endforeach; ?>
		</div>

        </div>

      </div>
    </section><!-- End Contact Section -->