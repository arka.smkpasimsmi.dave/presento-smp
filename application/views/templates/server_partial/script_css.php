<?php
  if (!$this->session->userdata('role') == 'admin') {
    $this->session->set_flashdata('warning', 'Mohon login terlebih dahulu!');
    Redirect('Page/login');
  }
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?= $title ?></title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="<?= base_url('assets/images/Pasim.png') ?>" type="image/x-icon" />

        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
        
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/icon-kit/dist/css/iconkit.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/ionicons/dist/css/ionicons.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/weather-icons/css/weather-icons.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/c3/c3.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/owl.carousel/dist/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/owl.carousel/dist/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/dist/css/theme.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/style.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/summernote/dist/summernote-bs4.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/datedropper/datedropper.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/magnific-popup/dist/magnific-popup.css">
        <script src="<?= base_url() ?>assets/server_assets/src/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/js/alerts.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/src/js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>    
    </head>
 <body>