<!-- ======= Contact Section ======= -->
<section id="contact" class="contact mt-5">
      <div class="container" data-aos="fade-up">

		<?php foreach($beranda as $data) : ?>
			<div class="section-title">
				<h2>Pendaftaran PPDB</h2>
				<p>Berikut merupakan halaman pendaftaran peserta didik baru <?= $data->nama_sekolah; ?>.</p>
			</div>
		<?php endforeach; ?>

        <div class="row" data-aos="fade-up" data-aos-delay="100">

			
			<div class="col-lg-6">
				<form method="POST" action="<?= base_url('Page/insertPpdb'); ?>" class="form-custom" enctype="multipart/form-data">
              <div class="form-group">
				<h6>Nama Siswa</h6>
					<input type="text" class="form-control" name="nama_siswa"  value="<?= set_value('nama_siswa'); ?>">
					<small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('nama_siswa'); ?></small>
			  </div>
              <div class="form-group">
				<h6>Ijazah</h6>
					<input type="file" style="border:1px solid #d1d4d6;width:100%;" name="ijazah" accept=".png, .jpg, .jpeg" required="required">
			  </div>
              <div class="form-group">
				<h6>SKU (Surat Keterangan Ujian)</h6>
					<input type="file" style="border:1px solid #d1d4d6;width:100%;" name="sku" accept=".png, .jpg, .jpeg" required="required">
			  </div>
              <div class="form-group">
				<h6>AKK (Akta Kelahiran)</h6>
					<input type="file" style="border:1px solid #d1d4d6;width:100%;" name="akk" accept=".png, .jpg, .jpeg" required="required">
			  </div>
              <div class="form-group">
				<h6>KK (Kartu Keluarga)</h6>
					<input type="file" style="border:1px solid #d1d4d6;width:100%;" name="kk" accept=".png, .jpg, .jpeg" required="required">
			  </div>
              <div class="form-group">
				<h6>Pernyataan (Surat Pernyataan)</h6>
					<input type="file" style="border:1px solid #d1d4d6;width:100%;" name="pernyataan" accept=".png, .jpg, .jpeg" required="required">
			  </div>
			<button type="submit">Kirim</button>
		</form>
	</div>
	
		<div class="col-lg-6">
			<?php foreach($ppdb as $data) : ?>
			<div class="row">
			<div class="col-md-12">
			<div class="info-box">
				<i class="bx bx-map"></i>
				<h3>Alamat Kami</h3>
				<p><?= $data->alamat ?></p>
			</div>
			</div>
			<div class="col-md-6">
			<div class="info-box mt-4">
				<i class="bx bx-envelope"></i>
				<h3>Email</h3>
				<p><?= $data->email_sekolah ?></p>
			</div>
			</div>
			<div class="col-md-6">
			<div class="info-box mt-4">
				<i class="bx bx-phone-call"></i>
				<h3>Kontak</h3>
				<p><?= $data->telepon ?></p>
			</div>
			</div>	
		</div>
			<?php endforeach; ?>
		</div>

        </div>

      </div>
    </section><!-- End Contact Section -->