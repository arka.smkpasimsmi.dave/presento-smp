<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eskul extends CI_Controller {

	public function detailEskul($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_eskul',$id)->row_array();
		$title['title'] = 'Detail Esktrakulikuler | '.$nama['judul'];
		$data = [
			'eskul'	=> $this->crud->getById('tb_m_eskul',$id)
			];
		$data['beranda'] = $this->crud->get('tb_m_beranda');	

		$this->load->view('templates/navbar', $title);
		$this->load->view('eskul/detail_eskul', $data);
		$this->load->view('templates/footer');
	}
}