<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_bkk extends CI_Controller {

	public function listSerapan()
	{
		$title['title'] = 'List Serapan Kerja';
		$data = [
			'serapan'	=> $this->crud->get('tb_m_bkk')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/bkk/list_serapan',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function listLowongan()
	{
		$title['title'] = 'List Lowongan Kerja';
		$data = [
			'lowongan'	=> $this->crud->get('tb_m_bkk')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/bkk/list_lowongan',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function detailSerapan($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_bkk',$id)->row_array();
		$title['title'] = 'Detail Serapan | '.$nama['judul'];
		$data = [
			'serapan'	=> $this->crud->getById('tb_m_bkk',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/bkk/detail_serapan.php',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function detailLowongan($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_bkk',$id)->row_array();
		$title['title'] = 'Detail Lowongan | '.$nama['judul'];
		$data = [
			'lowongan'	=> $this->crud->getById('tb_m_bkk',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/bkk/detail_lowongan.php',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function deleteBkk($id,$menu)
	{
		$this->crud->delete($id,'tb_m_bkk');
		$this->session->set_flashdata('success','Sukses hapus data!');
		if ($menu == 'serapan') {
			Redirect('Admin_bkk/listSerapan');
		}elseif ($menu== 'lowongan') {
			Redirect('Admin_bkk/listLowongan');
		}
	}

	public function insertLowongan()
	{
		$this->form_validation->set_rules('judul','Judul', 'required',
    			['required' => 'Judul harus diisi!']);
			$this->form_validation->set_rules('deskripsi','Deskripsi', 'required',
				['required' => 'Deskripsi harus diisi!']);
			$this->form_validation->set_rules('kota','Kota', 'required',
	    		['required' => 'Kota harus diisi!']);
			$this->form_validation->set_rules('alamat','Alamat', 'required',
	    		['required' => 'Alamat harus diisi!']);
			$this->form_validation->set_rules('no_telp','No.Telepon', 'required',
	    		['required' => 'No.Telepon harus diisi!']);
			$this->form_validation->set_rules('email_perusahaan','Email Perusahaan', 'required',
	    		['required' => 'Email Perusahaan harus diisi!']);	

			if ($this->form_validation->run()== false) {
				$title['title'] = 'List Lowongan Kerja';
				$data = [
					'lowongan'	=> $this->crud->get('tb_m_bkk')
					];

				$this->load->view('templates/server_partial/script_css',$title);
				$this->load->view('templates/server_partial/header');
				$this->load->view('templates/server_partial/sidebar');
				$this->load->view('server/front_end/bkk/list_lowongan',$data);
				$this->load->view('templates/server_partial/footer');
				$this->load->view('templates/server_partial/script_js');
			}else{
				$judul				= $this->input->post('judul');
				$deskripsi			= $this->input->post('deskripsi');
				$kota				= $this->input->post('kota');
				$alamat				= $this->input->post('deskripsi');
				$no_telp			= $this->input->post('no_telp');
				$email_perusahaan	= $this->input->post('email_perusahaan');

					$config['upload_path']		= './assets/images/bkk_images/';
					$config['allowed_types']	= 'jpg|png|jpeg';
					$config['encrypt_name']		=  TRUE;
					$this->load->library('upload', $config);

					if(!$this->upload->do_upload('foto')){
						$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
						Redirect('Admin_bkk/listLowongan');
					}else{
						$foto 	  = $this->upload->data('file_name');
						$data = [
							'judul'				=> $judul,
							'item'				=> $foto,
							'deskripsi'			=> $deskripsi,
							'kota'				=> $kota,
							'alamat'			=> $alamat,
							'no_telp'			=> $no_telp,
							'email_perusahaan'	=> $email_perusahaan,
							'kategori'			=> 'lowongan',
							'created_by'		=> 'ADMIN'
						];
						$this->crud->insert($data,'tb_m_bkk');
						$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
						Redirect('Admin_bkk/listLowongan');
				}
			}
	}

	public function insertSerapan()
	{
			$this->form_validation->set_rules('judul','Judul', 'required',
    			['required' => 'Judul harus diisi!']);
			$this->form_validation->set_rules('deskripsi','Deskripsi', 'required',
	    		['required' => 'Deskripsi harus diisi!']);
			$this->form_validation->set_rules('kota','Kota', 'required',
	    		['required' => 'Kota harus diisi!']);
			$this->form_validation->set_rules('alamat','Alamat', 'required',
	    		['required' => 'Alamat harus diisi!']);
			$this->form_validation->set_rules('no_telp','No.Telepon', 'required',
	    		['required' => 'No.Telepon harus diisi!']);
			$this->form_validation->set_rules('email_perusahaan','Email Perusahaan', 'required',
	    		['required' => 'Email Perusahaan harus diisi!']);

			if ($this->form_validation->run()== false) {
				$title['title'] = 'List Serapan Kerja';
				$data = [
					'serapan'	=> $this->crud->get('tb_m_bkk')
					];

				$this->load->view('templates/server_partial/script_css',$title);
				$this->load->view('templates/server_partial/header');
				$this->load->view('templates/server_partial/sidebar');
				$this->load->view('server/front_end/bkk/list_serapan',$data);
				$this->load->view('templates/server_partial/footer');
				$this->load->view('templates/server_partial/script_js');
			}else{
				$judul				= $this->input->post('judul');
				$deskripsi			= $this->input->post('deskripsi');
				$kota				= $this->input->post('kota');
				$alamat				= $this->input->post('deskripsi');
				$no_telp			= $this->input->post('no_telp');
				$email_perusahaan	= $this->input->post('email_perusahaan');

					$config['upload_path']		= './assets/images/bkk_images/';
					$config['allowed_types']	= 'jpg|png|jpeg';
					$config['encrypt_name']		=  TRUE;
					$this->load->library('upload', $config);

					if(!$this->upload->do_upload('foto')){
						$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
						Redirect('Admin_bkk/listSerapan');
					}else{
						$foto 	  = $this->upload->data('file_name');
						$data = [
							'judul'				=> $judul,
							'item'				=> $foto,
							'deskripsi'			=> $deskripsi,
							'kota'				=> $kota,
							'alamat'			=> $alamat,
							'no_telp'			=> $no_telp,
							'email_perusahaan'	=> $email_perusahaan,
							'kategori'			=> 'serapan',
							'created_by'		=> 'ADMIN'
						];
						$this->crud->insert($data,'tb_m_bkk');
						$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
						Redirect('Admin_bkk/listSerapan');
					}
		}
	}

	public function postEditSerapan($ids) {
		$id 			= ['id' => $ids];
		$judul			= $this->input->post('judul');
		$deskripsi 		= $this->input->post('deskripsi');
		$foto 			= $this->input->post('foto');
		$foto_lama 		= $this->input->post('foto_lama');

		if ($foto !== '') {
			$config['upload_path']		= './assets/images/bkk_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $judul.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}

			$data = [
				'item'				=> $foto,
				'judul'				=> $judul,
				'deskripsi'			=> $deskripsi
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'bkk_images');
			}
			$this->crud->edit($id,$data,'tb_m_bkk');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Admin_bkk/detailSerapan/').$ids);	
		
	}

	public function postEditLowongan($ids) {
		$id 			= ['id' => $ids];
		$judul			= $this->input->post('judul');
		$deskripsi 		= $this->input->post('deskripsi');
		$foto 			= $this->input->post('foto');
		$foto_lama 		= $this->input->post('foto_lama');

		if ($foto !== '') {
			$config['upload_path']		= './assets/images/bkk_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $judul.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}

			$data = [
				'item'				=> $foto,
				'judul'				=> $judul,
				'deskripsi'			=> $deskripsi
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'bkk_images');
			}
			$this->crud->edit($id,$data,'tb_m_bkk');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Admin_bkk/detailLowongan/').$ids);	
		
	}
}