<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_ppdb extends CI_Controller {
	public function index()
	{
		$title['title'] = 'PPDB';
		$data = [
			'calon'	=> $this->crud->get('tb_m_ppdb')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/ppdb/list_calon',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function insertPpdb()
	{
		$this->form_validation->set_rules('nama_lengkap','Nama Lengkap', 'required',
    		['required' => 'Nama Lengkap harus diisi!']);

		if ($this->form_validation->run()== false) {
			$title['title'] = 'PPDB';
			$data = [
				'calon'	=> $this->crud->get('tb_m_ppdb')
				];

			$this->load->view('templates/server_partial/script_css',$title);
			$this->load->view('templates/server_partial/header');
			$this->load->view('templates/server_partial/sidebar');
			$this->load->view('server/ppdb/list_calon',$data);
			$this->load->view('templates/server_partial/footer');
			$this->load->view('templates/server_partial/script_js');
		}else{
			$data = [];
			$nama_siswa	= $this->input->post('nama_lengkap');

				$config['upload_path']		= './assets/images/ppdb_images/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['encrypt_name']		=  TRUE;
				$this->load->library('upload', $config);

				if($this->upload->do_upload('ijazah')) {
					$fileData = $this->upload->data();
    				$data['ijazah'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('sku')) {
					$fileData = $this->upload->data();
    				$data['sku'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('akk')) {
					$fileData = $this->upload->data();
    				$data['akk'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('kk')) {
					$fileData = $this->upload->data();
    				$data['kk'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('pernyataan')) {
					$fileData = $this->upload->data();
    				$data['pernyataan'] = $fileData['file_name'];
				}
					
					$dataSiswa = [
						'nama_siswa'	=> $nama_siswa,
						'ijazah'		=> $data['ijazah'],
						'sku'			=> $data['sku'],
						'akk'			=> $data['akk'],
						'kk'			=> $data['kk'],
						'pernyataan'	=> $data['pernyataan']
					];
					$this->crud->insert($dataSiswa,'tb_m_ppdb'); 
					$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
					Redirect('Admin_ppdb');
		}
	}

	public function profilPpdb($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_ppdb',$id)->row_array();
		$title['title'] = 'Profil Calon Siswa | '.$nama['nama_siswa'];
		$data = [
			'calon'	=> $this->crud->getById('tb_m_ppdb',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/ppdb/profil_calon',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function deleteCalon($id)
	{
		$this->crud->delete($id,'tb_m_ppdb');
		$this->session->set_flashdata('success','Sukses hapus data!');
		Redirect('Admin_ppdb');
	}

	public function postEditPpdb($id)
	{
		$data = array();
		$id 					= ['id' => $id];
		$nama_siswa 			= $this->input->post('nama_siswa');
		$foto_lama_ijazah 		= $this->input->post('foto_lama_ijazah');
		$foto_lama_sku		 	= $this->input->post('foto_lama_sku');
		$foto_lama_akk 			= $this->input->post('foto_lama_akk');
		$foto_lama_kk 			= $this->input->post('foto_lama_kk');
		$foto_lama_pernyataan 	= $this->input->post('foto_lama_pernyataan');
		$foto_ijazah 			= $this->input->post('ijazah');
		$foto_sku	 			= $this->input->post('sku');
		$foto_akk	 			= $this->input->post('akk');
		$foto_akk				= $this->input->post('kk');
		$foto_pernyataan 		= $this->input->post('pernyataan');

		if ($foto_ijazah == '') {
			$config['upload_path']		= './assets/images/ppdb_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['encrypt_name']		=  TRUE;
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('ijazah')){
				$data['ijazah']			= $foto_lama_ijazah;
    		}else{
    			$fileData = $this->upload->data();
    			$data['ijazah'] = $fileData['file_name'];
   			}

		}
		if ($foto_sku == '') {
			$config['upload_path']		= './assets/images/ppdb_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['encrypt_name']		=  TRUE;
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('sku')){
				$data['sku']			= $foto_lama_sku;
    		}else{
    			$fileData = $this->upload->data();
    			$data['sku'] = $fileData['file_name'];
   			}

		}
		if ($foto_akk == '') {
			$config['upload_path']		= './assets/images/ppdb_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['encrypt_name']		=  TRUE;
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('akk')){
				$data['akk']			= $foto_lama_akk;
    		}else{
    			$fileData = $this->upload->data();
    			$data['akk'] = $fileData['file_name'];
   			}

		}
		if ($foto_kk == '') {
			$config['upload_path']		= './assets/images/ppdb_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['encrypt_name']		=  TRUE;
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('kk')){
				$data['kk']			= $foto_lama_kk;
    		}else{
    			$fileData = $this->upload->data();
    			$data['kk'] = $fileData['file_name'];
   			}

		}
		if ($foto_pernyataan == '') {
			$config['upload_path']		= './assets/images/ppdb_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['encrypt_name']		=  TRUE;
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('pernyataan')){
				$data['pernyataan'] 	= $foto_lama_pernyataan;
    		}else{
    			$fileData = $this->upload->data();
    			$data['pernyataan'] = $fileData['file_name'];
   			}

		}

			$dataSiswa = [
				'nama_siswa'	=> $nama_siswa,
				'ijazah'		=> $data['ijazah'],
				'sku'			=> $data['sku'],
				'akk'			=> $data['akk'],
				'kk'			=> $data['kk'],
				'pernyataan'	=> $data['pernyataan'],
				'created_by'	=> 'ADMIN'
			];

			$this->crud->edit($id,$dataSiswa,'tb_m_ppdb');
			$this->session->set_flashdata('success','Sukses Edit data PPDB!');
			Redirect('Admin_ppdb/');
		}
}